# SSO Permissions

# Setting Up Virtual Environment
```
# Using Python 2.7
$ virtualenv .venv

# activate the virtual env
$ source .venv/bin/activate

# install packages
$ pip install -r requirements.txt
```

## Workflow
- upload cfn permission sets
- load account permission files
- get sso accounts
- loop through each permission set in each account
- get existing users
- get updated users
- get users to assign permissions too
- get users to remove permissions from
- remove user permissions
- update user permissions