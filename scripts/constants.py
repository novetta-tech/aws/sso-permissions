from __future__ import absolute_import
from os import path

LOCAL_ASSIGNMENTS_PATH = path.abspath('./permission_assignments')
LOCAL_PERMISSION_SETS_PATH = path.abspath('./permission_sets')

IDENTITY_STORE = 'd-9a673946dd'
SSO_INSTANCE = 'arn:aws:sso:::instance/ssoins-66842681427e362d'

ACCOUNTS = {
  'Log Archive': '951077220833',
  'Audit': '115033207217'
}
