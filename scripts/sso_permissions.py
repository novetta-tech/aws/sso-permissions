from __future__ import absolute_import
from constants import ACCOUNTS, IDENTITY_STORE, LOCAL_ASSIGNMENTS_PATH, LOCAL_PERMISSION_SETS_PATH, SSO_INSTANCE
from os import listdir, path
import boto3
import json
import os
import stringcase
import yaml

ids_client = boto3.client('identitystore')
sso_client = boto3.client('sso-admin')

# Read local permission assignments
# Validate permission assignment accounts
# Validate permission assignment permission sets
# Get all users' user ids
# Get all groups' group ids
# For each permission set in each account update the user assignment
# For each permission set in each account update the groups assignment

def main():
  assignments = get_repo_permission_assignments()
  accounts_ids = assignments.keys()
  print("New assignment account ids: {}".format(accounts_ids))
  validate_accounts(AccountIds=accounts_ids)
  repo_permission_sets = list(set(reduce(lambda i,j: i+j, map(lambda x: x.keys(), assignments.values()))))
  permission_sets = dict((x['Name'], x['Arn']) for x in get_permission_sets())
  print("New assignment permission sets: {}".format(repo_permission_sets))
  validate_permission_sets(PermissionSets=repo_permission_sets)
  perm_set_identities = reduce(lambda i,j: i+j, map(lambda x: x.values(), assignments.values()))
  usernames = list(set(reduce(lambda i,j: i+j, map(lambda x: x['users'], perm_set_identities))))
  groupnames = list(set(reduce(lambda i,j: i+j, map(lambda x: x['groups'], perm_set_identities))))
  users = describe_users(UserNames=usernames)
  print(users)
  groups = describe_groups(GroupNames=groupnames)
  print(groups)
  
  for acct in assignments:
    for perm_set in assignments[acct]:
      for user in assignments[acct][perm_set]['users']:
        print("Assigning permission {} to user {} in {}".format(perm_set, user, acct))
        response = create_assignment(
          AccountId=acct,
          PermissionSetArn=permission_sets[perm_set],
          PrincipalType='USER',
          PrincipalId=users[user]
        )
      for group in assignments[acct][perm_set]['groups']:
        print("Assigning permission {} to group {} in {}".format(perm_set, group, acct))
        response = create_assignment(
          AccountId=acct,
          PermissionSetArn=permission_sets[perm_set],
          PrincipalType='GROUP',
          PrincipalId=groups[group]
        )

# Wrapper to simplify making account permission assignments
def create_assignment(AccountId, PermissionSetArn, PrincipalType, PrincipalId):
  return sso_client.create_account_assignment(
          InstanceArn=SSO_INSTANCE,
          TargetId=AccountId,
          TargetType='AWS_ACCOUNT',
          PermissionSetArn=PermissionSetArn,
          PrincipalType=PrincipalType,
          PrincipalId=PrincipalId
        )

# Get dict of users ids and names based on there usernames
def describe_users(UserNames=[]):
  user_filter = []
  for username in UserNames:
    user_filter.append({
      "AttributePath": "UserName",
      "AttributeValue": username
    })

  response = ids_client.list_users(
    IdentityStoreId=IDENTITY_STORE,
    MaxResults=len(user_filter),
    Filters=user_filter
  )
  return dict((x["UserName"].encode('utf-8'), x["UserId"].encode('utf-8')) for x in response["Users"])
  # return map(lambda x: {x["UserName"].encode('utf-8'):x["UserId"].encode('utf-8')}, response["Users"])

# Get dict of groups ids and names based on their names
def describe_groups(GroupNames=[]):
  group_filter = []
  for name in GroupNames:
    group_filter.append({
      "AttributePath": "DisplayName",
      "AttributeValue": name
    })

  response = ids_client.list_groups(
    IdentityStoreId=IDENTITY_STORE,
    MaxResults=len(group_filter),
    Filters=group_filter
  )
  return dict((x["DisplayName"].encode('utf-8'), x["GroupId"].encode('utf-8')) for x in response["Groups"])
  # return map(lambda x: {x["DisplayName"].encode('utf-8'):x["GroupId"].encode('utf-8')}, response["Groups"])

# Valdiate list of accounts exist, using ids and based on ACCOUNTS config value
def validate_accounts(AccountIds=[]):
  for acct_id in AccountIds:
    ACCOUNTS.values().index(acct_id)

# Validate list of permission sets existin in AWS (based on names)
def validate_permission_sets(PermissionSets=[]):
  aws_permission_sets = get_permission_sets()
  for perm_set in PermissionSets:
    map(lambda x: x['Name'], aws_permission_sets).index(perm_set)

# Get all account permission set assignments from current local repo
def get_repo_permission_assignments():
  result = {}

  for acct_dir in listdir(LOCAL_ASSIGNMENTS_PATH):
    full_acct_path = path.join(LOCAL_ASSIGNMENTS_PATH, acct_dir)

    for permission_file in listdir(full_acct_path):
      permission_file_path = path.join(full_acct_path, permission_file)
      yaml_file = open(permission_file_path)
      acct_permission_set_assignments = yaml.load(yaml_file, Loader=yaml.FullLoader)
      account_id = str(acct_permission_set_assignments['account']['id'])
      permission_set = acct_permission_set_assignments['permission_set']

      if not hasattr(result, account_id):
        result[account_id] = {}

      if not hasattr(result[account_id], permission_set):
        result[account_id][permission_set] = {"users":[],"groups":[]}

      result[account_id][permission_set]["users"] = list(set().union(result[account_id][permission_set]["users"], acct_permission_set_assignments["users"]))
      result[account_id][permission_set]["groups"] = list(set().union(result[account_id][permission_set]["groups"], acct_permission_set_assignments["groups"]))

  return result

# Get existing permission sets AWS
def get_permission_sets():
  results = []
  response = sso_client.list_permission_sets(InstanceArn=SSO_INSTANCE)
  permission_set_arns = response['PermissionSets']

  for arn in permission_set_arns:
    response = sso_client.describe_permission_set(
      InstanceArn=SSO_INSTANCE,
      PermissionSetArn=arn
    )
    results.append({
      "Name": response['PermissionSet']["Name"].encode('utf-8'),
      "Arn": response['PermissionSet']["PermissionSetArn"].encode('utf-8')
    })

  return results


if __name__ == '__main__':
  main()












# local_permissions_sets = local_perms.get_permission_sets()
# print(local_permissions_sets)

# local_permission_assignments = local_perms.get_permission_assignments()
# print(local_permission_assignments)

# users

# # validate permission set exists
# for acct in local_permission_assignments:
#   for perm_set in local_permission_assignments[acct]:

#     for user in local_permission_assignments[acct][perm_set]["users"]:
#       print(user)
